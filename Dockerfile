FROM python:3.11-slim

RUN pip install --upgrade pip

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .env .env
COPY hs-fastapi.py hs-fastapi.py

EXPOSE 8000

CMD ["python3", "hs-fastapi.py"]